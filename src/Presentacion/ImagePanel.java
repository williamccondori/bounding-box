package Presentacion;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;
  
//public class EjemploZoom
//{
//    public static void main(String[] args)
//    {
//        ImagePanel panel = new ImagePanel();
//        ImageZoom zoom = new ImageZoom(panel);
//        JFrame f = new JFrame();
//        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        
//        
//        
//        f.getContentPane().add(zoom.getUIPanel(), "North");
//        f.getContentPane().add(new JScrollPane(panel));
//        
//        
//        f.setSize(600,600);
//        f.setLocation(200,200);
//        f.setVisible(true);
//    }
//}
  
public class ImagePanel extends JPanel{
    BufferedImage image;
    double scale;
    private int x, y, x2, y2;
    Graphics2D g2; 
  
    public ImagePanel()
    {
        x = y = x2 = y2 = 0;
        loadImage();
        this.image = getImage();
        
        scale = 1.0;
        setBackground(Color.black);
        MyMouseListener listener = new MyMouseListener();
        addMouseListener(listener);
        addMouseMotionListener(listener);
    }
    public void drawPerfectRect(Graphics g, int x, int y, int x2, int y2) {
        if (x == x2 && y == y2) {

        } else {
            System.out.println("Dibujando");
            int px = Math.min(x, x2);
            int py = Math.min(y, y2);
            int pw = Math.abs(x - x2);
            int ph = Math.abs(y - y2);

            //drawCenteredCircle(g, x, y, 4);
            //drawCenteredCircle(g, x, y2, 4);
            //drawCenteredCircle(g, x2, y, 4);
            //drawCenteredCircle(g, x2, y2, 4);
            g.drawString("SHIP", x, y);

            g.drawRect(px, py, pw, ph);

        }
    }
    
    class MyMouseListener extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            AffineTransform at = AffineTransform.getScaleInstance(1.0/scale, 1.0/scale);
            Point2D p = at.transform(e.getPoint(), null);
            setStartPoint((int)p.getX(), (int)p.getY());
//            setStartPoint(e.getX(), e.getY());
        }

        @Override
        public void mouseDragged(MouseEvent e) {
//            setEndPoint(e.getX(), e.getY());
            AffineTransform at = AffineTransform.getScaleInstance(1.0/scale, 1.0/scale);
            Point2D p = at.transform(e.getPoint(), null);
            setEndPoint((int)p.getX(), (int)p.getY());

            repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
//            setEndPoint(e.getX(), e.getY());
            AffineTransform at = AffineTransform.getScaleInstance(1.0/scale, 1.0/scale);
            Point2D p = at.transform(e.getPoint(), null);
            setEndPoint((int)p.getX(), (int)p.getY());

            repaint();
            System.out.println("----------");
            System.err.println(x);
            System.err.println(y);

            System.out.println(x2);
            System.out.println(y2);
        }
    }
    
    public void setStartPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setEndPoint(int x, int y) {
        x2 = (x);
        y2 = (y);
    }
  
    protected void paintComponent(Graphics g)
    {
        super.paintComponents(g);
        
//        Graphics2D g2 = (Graphics2D)g;
//        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
//                            RenderingHints.VALUE_INTERPOLATION_BICUBIC);
//        int w = getWidth();
//        int h = getHeight();
//        int imageWidth = image.getWidth();
//        int imageHeight = image.getHeight();
//        double x1 = (w-scale * imageWidth)/2;
//        double y1 = (h-scale * imageHeight)/2;
//        AffineTransform at = AffineTransform.getTranslateInstance(x1,y1);
//        at.scale(scale, scale);
//        
//        
//        g2.drawRenderedImage(image, at);
//        
//        g.setColor(Color.GREEN);
//        
//        drawPerfectRect(g, x, y, x2, y2);

   
   
//        AffineTransform at = AffineTransform.getScaleInstance(1.0/scale, 1.0/scale);
//        g2.drawRenderedImage(image, at);
//        
        Graphics2D g2 = (Graphics2D) g;
	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	g2.scale(scale,scale);
	g2.drawImage(image, 0, 0, null);
	//g2.scale(1.0, 1.0);
        g2.setColor(Color.GREEN);
     

//        int x_ = (int)((image.getWidth())-(scale * image.getWidth()));
//        int y_ = (int)((image.getHeight())-(scale * image.getHeight()));
        
   //     drawPerfectRect(g2, x , y, x2, y2);
        
          System.out.println("Dibujando");
            int px = Math.min(x, x2);
            int py = Math.min(y, y2);
            int pw = Math.abs(x - x2);
            int ph = Math.abs(y - y2);

            //drawCenteredCircle(g, x, y, 4);
            //drawCenteredCircle(g, x, y2, 4);
            //drawCenteredCircle(g, x2, y, 4);
            //drawCenteredCircle(g, x2, y2, 4);
            g2.drawString("SHIP", x, y);

            g2.drawRect(px, py, pw, ph);
            g2.dispose();

    }
  
    public BufferedImage getImage(){
    
       double xscale = (double) 3000/ this.image.getWidth();
       double yscale = (double) 3000/ this.image.getHeight();
       double scale = Math.min(xscale, yscale);
       int w = (int) (scale*this.image.getWidth());
       int h = (int) (scale*this.image.getHeight());

       BufferedImage scaled = new BufferedImage(w, h, this.image.getType());
       Graphics2D g2 = scaled.createGraphics()  ;
       g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

        AffineTransform at = AffineTransform.getScaleInstance(1.0/scale, 1.0/scale);
        g2.drawRenderedImage(image, at);
        
          g2.dispose();

        return scaled;
    }
    /**
     * For the scroll pane.
     */
    public Dimension getPreferredSize()
    {
        int w = (int)(scale * image.getWidth());
        int h = (int)(scale * image.getHeight());
        return new Dimension(w, h);
    }
  
    public void setScale(double s)
    {
        scale = s;
        revalidate();      // update the scroll pane
        repaint();
    }
    public void zoomOut(){
        double num= this.scale - 0.01;
        this.setScale(num);
    }
    public void zoomIn(){
        double num= this.scale + 0.01;
        this.setScale(num);
    }
  
    private void loadImage()
    {
        String fileName = "100000001.bmp";
        try
        {
            URL url = getClass().getResource(fileName);
            this.image = ImageIO.read(url);
        }
        catch(MalformedURLException mue)
        {
            System.out.println("URL trouble: " + mue.getMessage());
        }
        catch(IOException ioe)
        {
            System.out.println("read trouble: " + ioe.getMessage());
        }
    }
}
  
//class ImageZoom
//{
//    ImagePanel imagePanel;
//  
//    public ImageZoom(ImagePanel ip)
//    {
//        imagePanel = ip;
//    }
//  
//    public JPanel getUIPanel()
//    {
//        SpinnerNumberModel model = new SpinnerNumberModel(1.0, 0.1, 1.4, .01);
//        final JSpinner spinner = new JSpinner(model);
//        spinner.setPreferredSize(new Dimension(45, spinner.getPreferredSize().height));
//        spinner.addChangeListener(new ChangeListener()
//        {
//            public void stateChanged(ChangeEvent e)
//            {
//                float scale = ((Double)spinner.getValue()).floatValue();
//                imagePanel.setScale(scale);
//            }
//        });
//        JPanel panel = new JPanel();
//        panel.add(new JLabel("scale"));
//        panel.add(spinner);
//        return panel;
//    }
//}