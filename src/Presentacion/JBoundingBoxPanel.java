/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author William
 */
public class JBoundingBoxPanel extends JPanel {

    private int x, y, x2, y2;

    public JBoundingBoxPanel() {
        initPanel();
    }

    private void initPanel() {
        x = y = x2 = y2 = 0; //
        scala=1.0;
        MyMouseListener listener = new MyMouseListener();
        addMouseListener(listener);
        addMouseMotionListener(listener);
        //_image = null;
        //URL imageUrl = getClass().getResource("/Presentacion/100000001.bmp");
        //System.out.println(imageUrl);
        //image = ImageIO.read(new File(imageUrl.getPath()));

    }
    
    public void drawPerfectRect(Graphics g, int x, int y, int x2, int y2) {
        if (x == x2 && y == y2) {

        } else {
            System.out.println("Dibujando");
            int px = Math.min(x, x2);
            int py = Math.min(y, y2);
            int pw = Math.abs(x - x2);
            int ph = Math.abs(y - y2);

            //drawCenteredCircle(g, x, y, 4);
            //drawCenteredCircle(g, x, y2, 4);
            //drawCenteredCircle(g, x2, y, 4);
            //drawCenteredCircle(g, x2, y2, 4);
            g.drawString("SHIP", x, y);

            g.drawRect(px, py, pw, ph);

        }
    }


    class MyMouseListener extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            setStartPoint(e.getX(), e.getY());
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            setEndPoint(e.getX(), e.getY());
            repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            setEndPoint(e.getX(), e.getY());
            repaint();
            System.out.println("----------");
            System.err.println(x);
            System.err.println(y);

            System.out.println(x2);
            System.out.println(y2);
        }
    }

    public void setStartPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setEndPoint(int x, int y) {
        x2 = (x);
        y2 = (y);
    }
    
     @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        if (this.con_foto){
//            //g.drawImage(_image, 0, 0, this);
//            Graphics2D g2 = (Graphics2D) g;
//            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
//                    RenderingHints.VALUE_INTERPOLATION_BICUBIC);
//            int w = getWidth();
//            int h = getHeight();
//            int imageWidth = _image.getWidth();
//            int imageHeight = _image.getHeight();
//            double x = (w - scala * imageWidth) / 2;
//            double y = (h - scala * imageHeight) / 2;
//            AffineTransform at = AffineTransform.getTranslateInstance(x, y);
//            at.scale(scala, scala);
//            g2.drawRenderedImage(_image, at);
              Imagen_en_memoria = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
                g2D = Imagen_en_memoria.createGraphics();
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //se aÃ±ade la foto
        g2D.drawImage(FOTO_tmp,0, 0, FOTO_tmp.getWidth(this), FOTO_tmp.getHeight(this), this);
        g2.drawImage(Imagen_en_memoria, 0, 0, this);
        }
        g.setColor(Color.GREEN);
        drawPerfectRect(g, x, y, x2, y2);
    }
    
    private BufferedImage _image;
    double scala;
    private Image FOTO_ORIGINAL;
    private Image FOTO_tmp;
    private BufferedImage Imagen_en_memoria;
    private Graphics2D g2D;
    private boolean con_foto = false;   
 
    private int valEscalaX=0;
    private int valEscalaY=0;


    public void setImagee(BufferedImage f){
//        _image=image;
//        this.setPreferredSize(new Dimension(_image.getWidth(), _image.getHeight()));
//        repaint();
        this.FOTO_ORIGINAL = f;
        this.FOTO_tmp = f;
        this.setSize(f.getWidth(),f.getHeight());
        this.setVisible(true);
        this.con_foto=true;
        
    }
    public void Aumentar(int Valor_Zoom){
        //se calcula el incremento
        valEscalaX =  (int) (FOTO_tmp.getWidth(this) * escala(Valor_Zoom) );
        valEscalaY =  (int) (FOTO_tmp.getHeight(this) * escala(Valor_Zoom) );
        //se escala la imagen sumado el nuevo incremento
        this.FOTO_tmp = FOTO_tmp.getScaledInstance((int) (FOTO_tmp.getWidth(this) + valEscalaX), (int) (FOTO_tmp.getHeight(this) + valEscalaY), Image.SCALE_AREA_AVERAGING);
        resize();
    }
    public void Disminuir(int Valor_Zoom){
        valEscalaX =  (int) (FOTO_tmp.getWidth(this) * escala(Valor_Zoom) );
        valEscalaY =  (int) (FOTO_tmp.getHeight(this) * escala(Valor_Zoom) );
        this.FOTO_tmp = FOTO_tmp.getScaledInstance((int) (FOTO_tmp.getWidth(this) - valEscalaX), (int) (FOTO_tmp.getHeight(this) - valEscalaY), Image.SCALE_AREA_AVERAGING);
        resize();
     }
 
    private float escala(int v){
        return  v/100f;
    }
 
    public void Restaurar(){
        this.FOTO_tmp = this.FOTO_ORIGINAL;
        resize();
    }
 
    private void resize(){
        this.setSize(FOTO_tmp.getWidth(this),FOTO_tmp.getHeight(this));
    }
    
    public void zoomIn(){
            int w=_image.getWidth();
            int h=_image.getHeight();
            Image i=_image;
            _image = (BufferedImage) zoomImage(w+10,h+10,i); 
            this.setPreferredSize(new Dimension(_image.getWidth(), _image.getHeight()));
            repaint();
    }
    
    public void zoomOut(){
            int w=_image.getWidth();
            int h=_image.getHeight();
            Image i=_image;
            _image = (BufferedImage) zoomImage(w-10,h-10,i); 
            this.setPreferredSize(new Dimension(_image.getWidth(), _image.getHeight()));
            repaint();
    }
    private Image zoomImage(int w, int h , Image img){
        BufferedImage buf=new BufferedImage(w,h,BufferedImage.TYPE_INT_BGR);
        Graphics2D grf=buf.createGraphics();
        grf.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR );
        grf.drawImage(img, 0, 0,w,h,null);
        grf.dispose();
        return buf;
    }
}
