package Presentacion;

import javax.swing.*;

import java.awt.*;

import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class DrawRect extends JPanel {

    int x, y, x2, y2;

    public static void main(String[] args) {
        JFrame f = new JFrame("Draw Box Mouse 2");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setContentPane(new DrawRect());
        f.setSize(300, 300);
        f.setVisible(true);
    }

    DrawRect() {
        initPanel();
    }

    public void setStartPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setEndPoint(int x, int y) {
        x2 = (x);
        y2 = (y);
    }

    public void drawPerfectRect(Graphics g, int x, int y, int x2, int y2) {
        if (x == x2 && y == y2) {

        } else {
            System.out.println("Dibujando");
            int px = Math.min(x, x2);
            int py = Math.min(y, y2);
            int pw = Math.abs(x - x2);
            int ph = Math.abs(y - y2);

            //drawCenteredCircle(g, x, y, 4);
            //drawCenteredCircle(g, x, y2, 4);
            //drawCenteredCircle(g, x2, y, 4);
            //drawCenteredCircle(g, x2, y2, 4);
            g.drawString("SHIP", x, y);

            g.drawRect(px, py, pw, ph);

        }
    }

    public void drawCenteredCircle(Graphics g, int x, int y, int r) {
        x = x - (r / 2);
        y = y - (r / 2);
        g.fillOval(x, y, r, r);
    }

    private BufferedImage image;

    private void initPanel() {
        x = y = x2 = y2 = 0; // 
        MyMouseListener listener = new MyMouseListener();
        addMouseListener(listener);
        addMouseMotionListener(listener);

        try {
            URL imageUrl = getClass().getResource("/Presentacion/100000001.bmp");
            System.out.println(imageUrl);
            image = ImageIO.read(new File(imageUrl.getPath()));
        } catch (IOException ex) {
            Logger.getLogger(DrawRect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    class MyMouseListener extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            setStartPoint(e.getX(), e.getY());
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            setEndPoint(e.getX(), e.getY());
            repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            setEndPoint(e.getX(), e.getY());
            repaint();
            System.out.println("----------");
            System.err.println(x);
            System.err.println(y);

            System.out.println(x2);
            System.out.println(y2);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
        g.setColor(Color.GREEN);
        drawPerfectRect(g, x, y, x2, y2);
    }

}
